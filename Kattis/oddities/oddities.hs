oddity :: IO ()
oddity = do
  n <- readLn
  putStrLn $ show n ++ " is " ++ case even n of
    False -> "odd"
    True -> "even"

main :: IO ()
main = do
  n <- readLn
  sequence_ [oddity | _ <- [1..n]]
