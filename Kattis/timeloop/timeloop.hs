main :: IO ()
main = do
  n <- readLn
  sequence_ [putStrLn $ show i ++ " Abracadabra" | i <- [1..n]]
