import Control.Monad

fizzbuzz :: Int -> Int -> Int -> IO ()
fizzbuzz d1 d2 n = do
  when (divBy d1) $ putStr "Fizz"
  when (divBy d2) $ putStr "Buzz"
  if (not (divBy d1) && not (divBy d2))
    then print n
    else putChar '\n'
  where divBy d = n `rem` d == 0

main :: IO ()
main = do
  [d1, d2, l] <- map read . words <$> getLine
  sequence_ [fizzbuzz d1 d2 n | n <- [1..l]]
