main :: IO ()
main = do
  x <- readLn
  y <- readLn
  putStrLn $ case (x > 0, y > 0) of
    (False, False) -> "3"
    (False, True)  -> "2"
    (True,  False) -> "4"
    (True,  True)  -> "1"
