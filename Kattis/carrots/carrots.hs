{-# LANGUAGE ScopedTypeVariables #-}

main :: IO ()
main = do
  [_, n] :: [Int] <- map read . words <$> getLine
  print n
