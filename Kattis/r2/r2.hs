main :: IO ()
main = do
  [r1, s] <- map read . words <$> getLine
  print $ r1 + 2 * (s - r1)
