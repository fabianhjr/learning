{-# LANGUAGE ScopedTypeVariables #-}

import Control.Monad
import Data.Int
import System.IO

main :: IO ()
main = do
  [x, y] :: [Int64] <- map read . words <$>  getLine
  print . abs $ x - y
  end <- isEOF
  unless end main
