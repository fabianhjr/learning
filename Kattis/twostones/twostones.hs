main :: IO ()
main = do
  n <- readLn
  let winner = if even n then "Bob" else "Alice"
  putStrLn winner
