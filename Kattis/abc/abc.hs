import Data.List
import Data.Maybe

order :: [Int] -> String -> [Int]
order n [] = []
order n (o:os) = (n !! pos) : order n os
  where pos = fromJust . elemIndex o $ "ABC"

main :: IO ()
main = do
  l@[a, b, c] <- sort . map read . words <$> getLine
  o <- getLine
  putStrLn . unwords $ show <$> order l o
