main :: IO ()
main = do
  _ <- getLine
  s <- length . filter (< 0) . map read . words <$> getLine
  print s
