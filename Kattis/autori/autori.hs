autori :: String -> String
autori s
  | not . null $ s = head s : autori (drop 1 . dropWhile (/= '-') $ tail s)
  | otherwise = ""

main :: IO ()
main = do
  input <- getLine
  putStrLn . autori $ input
