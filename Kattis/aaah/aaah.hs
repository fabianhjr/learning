main :: IO ()
main = do
  can <- length <$> getLine
  req <- length <$> getLine
  putStrLn $ if can >= req then "go" else "no"
