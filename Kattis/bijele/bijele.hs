main :: IO ()
main = do
  input <- map read . words <$> getLine
  putStrLn . unwords $ show <$> diff input
  where diff input = negate <$> zipWith (-) input [1, 1, 2, 2, 2, 8]
