#include <stdio.h>

int sum(int step, int limit) {
    return (step * (limit / step) * (limit / step + 1)) / 2;
}

int main() {
    printf("%d", sum(3, 999) + sum(5, 999) - sum(15, 999)); 
    return 0;
}
