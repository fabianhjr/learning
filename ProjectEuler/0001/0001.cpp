#include <iostream>

using namespace std;

int sum(int step, int limit) {
    return (step * (limit / step) * (limit / step + 1)) / 2;
}

int main() {
    int sum3 = sum(3, 999);
    int sum5 = sum(5, 999);
    int sum15 = sum(15, 999);
    int sumT = sum3 + sum5 - sum15;
    cout << sumT << endl;
    return 0;
}
