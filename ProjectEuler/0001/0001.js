function sum(step, limit) {
    return (step * Math.floor(limit / step) * Math.floor(limit / step + 1)) / 2
}

console.log(sum(3, 999) + sum(5, 999) - sum(15, 999))
