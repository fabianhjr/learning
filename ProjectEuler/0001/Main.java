class Main {
    public static int sum(int step, int limit) {
        return (step * (limit / step) * (limit / step + 1)) / 2;
    }
    
    public static void main(String[] args) {
       System.out.println(sum(3, 999) + sum(5, 999) - sum(15,999)); 
    }
}
