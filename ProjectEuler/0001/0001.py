def sum(step, limit):
    return (step * (limit // step) * (limit // step + 1)) // 2

print(sum(3, 999) + sum(5,999) - sum(15,999))
