package main

import (
	"bufio"
	"os"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	console := bufio.NewWriter(os.Stdout)

	for scanner.Scan() {
		if (scanner.Text() == "42") { break }
		console.WriteString(scanner.Text())
		console.WriteRune('\n')
		console.Flush()
	}
}
