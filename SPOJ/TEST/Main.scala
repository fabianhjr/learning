object Main {
  var input: String = "";

  def main(args: Array[String]): Unit = {
    while(input != "42") {
      input = io.StdIn.readLine()
      if(input != "42") println(input)
    }
  }
}
