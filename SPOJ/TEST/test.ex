defmodule Test do
  def main() do
    input = IO.gets("")

    if input != "42\n" do
      IO.write(input)
      main()
    end
  end
end

Test.main()
