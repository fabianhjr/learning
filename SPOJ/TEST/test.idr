main : IO ()
main = do
  input <- getLine
  if input /= "42" then recurse input else pure ()
  where
    recurse : String -> IO ()
    recurse input = do
      putStrLn input
      main
