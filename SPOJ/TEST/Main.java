import java.util.Scanner;

public class Main {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String input;

        while(true) {
            input = sc.nextLine();
            if (input.equals("42")) break;
            System.out.println(input);
        }
    }
}
