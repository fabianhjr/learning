(defn main []
  (def input (read-line))
  (if (not (= input "42"))
    (do
      (println input)
      (main))))

(main)
