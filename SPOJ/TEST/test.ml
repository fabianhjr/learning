let rec main = fun x ->
  match read_line () with
    | "42" -> ()
    | other -> print_endline other; main ();;

main ()
