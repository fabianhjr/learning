const readline = require('readline');

const rl = readline.createInterface({input: process.stdin});

async function main() {
    for await (const line of rl) {
        if (line == "42") break;
        console.log(line);
    }
}

main();
