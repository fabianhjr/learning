#include <stdio.h>

char *input = NULL;
size_t length = 0;

int main() {
  while(1) {
    getline(&input, &length, stdin);
    if (length > 2 && input[0] == '4' && input[1] == '2' && input[2] == '\n') break;
    fputs(input, stdout);
  }

  return 0;
}
