(use-modules (ice-9 textual-ports) (ice-9 rdelim))

(define main
  (lambda ()
    (define input (get-line (current-input-port)))
    (unless (equal? input "42")
      (begin
        (display input)
        (newline)
        (main)
        ))))

(main)
