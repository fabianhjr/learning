(defun main ()
  (let ((input (read-line)))
    (unless (string= input "42")
      (write-line input)
      (main))))

(main)
