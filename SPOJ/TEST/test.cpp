#include <iostream>

using namespace std;

int input = 0;

int main() {
  while(true) {
    cin >> input;
    if (input == 42) break;
    cout << input << endl;
  }

  return 0;
}
