use std::io;

fn main() {
    let mut input;
    let stdin = io::stdin();

    loop {
        input = String::new();
        match stdin.read_line(&mut input) {
            Ok(_) => {
                if input == "42\n" {
                    break;
                }
                print!("{}", input);
            },

            Err(_) => break,
        }
    }
}
