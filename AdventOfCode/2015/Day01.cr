input = gets
floor = 0
arrived_basement = false
first_basement = 0

unless input.nil?
  input.each_char do |c|
    if floor < 0
      arrived_basement = true
    end

    unless arrived_basement
      first_basement += 1
    end

    case c
    when '('
      floor += 1
    when ')'
      floor -= 1
    end
  end
end

puts "Ended up at floor ##{floor}"

if arrived_basement
  puts "First time on basement on instruction ##{first_basement}"
end
